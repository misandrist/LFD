module Algebra.Linreg where

import Data.Packed.Matrix
import Numeric.LinearAlgebra.Algorithms
import Numeric.Container

data Sample = Sample {
    y :: Double,
    x1 ::Double,
    x2 :: Double
    } deriving Show

sampleVec :: [Sample] -> Matrix Double
sampleVec samples = fromLists $ map (\(Sample y x1 x2) -> [y, 1.0, x1, x2]) samples

xVec :: Matrix Double -> Matrix Double
xVec svec = dropColumns 1 svec

yVec :: Matrix Double -> Matrix Double
yVec svec = takeColumns 1 svec

wVec :: Matrix Double -> Matrix Double -> Matrix Double
wVec xvec yvec = (pinv $ (ctrans xvec) <> xvec) <> (ctrans xvec) <> yvec

