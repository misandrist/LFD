{-# LANGUAGE BangPatterns #-}

import System.Random
import Statistics.Sample as S
import qualified Data.Vector as V
import Control.Monad.IO.Class
import Data.List as L
import System.Environment
import Control.Monad.Random
import Control.Monad

flipCoin :: (RandomGen g) => Int -> Rand g [Int]
flipCoin n = sequence (replicate n $ getRandomR (0, 1))

avgFlips :: [Int] -> Double
avgFlips flips = average $ map (\x -> fromIntegral x :: Double) flips

flipCoins :: (RandomGen g) => Int -> Int -> Rand g [Double]
flipCoins m n = do
    coins <- sequence (replicate m $ flipCoin n)
    return $! map avgFlips coins

runStats :: (RandomGen g) => Int -> Int -> Rand g (Double, Double, Double)
runStats m n = do
    s <- flipCoins m n
    
    r <- getRandomR(0, m - 1)
    
    return $ (s!!0, s!!r, minimum s)

average :: [Double] -> Double
average l = sum l / (fromIntegral $ length l :: Double)

run :: (RandomGen g) => Int -> Rand g (Double, Double, Double)
run n = do
    s <- sequence (replicate n $ runStats 1000 10)

    return $ (\(g, h, i) -> (g / (fromIntegral n :: Double),
                             h / (fromIntegral n :: Double),
                             i / (fromIntegral n :: Double)))
      $ foldl' (\(!a, !b, !c) (!d, !e, !f) -> (a + d, b + e, c + f)) (0.0, 0.0, 0.0) s
    
main = do
    args <- getArgs
    let count = read $ args!!0 :: Int
    (firsts, rands, mins) <- evalRandIO $ run count 

    putStr "v1: "
    print firsts
    putStr "vrand: "
    print rands
    putStr "vmin: "
    print mins

