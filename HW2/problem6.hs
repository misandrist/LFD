import Control.Monad.Random
import Control.Monad
import Data.Packed.Matrix
import Numeric.LinearAlgebra.Algorithms
import Numeric.Container
import System.Environment

data Point = Point Double Double
             deriving Show
             
coord :: (RandomGen g) => Rand g Point
coord = do
    x <- getRandom
    y <- getRandom

    return $ Point x y

line :: Point -> Point -> (Double -> Double)
line (Point x1 y1) (Point x2 y2) = (\x -> ((y2 - y1) / (x2 - x1)) * (x - x1) + y1)

lineRel :: (Double -> Double) -> Point -> Integer
lineRel l (Point x y) = if l x >= y then -1 else 1

coords :: (RandomGen g) => Int -> Rand g [Point]
coords n = sequence (replicate n coord)

samples :: (Double -> Double) -> [Point] -> [(Integer, Point)]
samples l ps = map (\p -> ((lineRel l p), p)) ps

experiment :: Int -> IO (Matrix Double, Double)
experiment n = do
    p0 <- evalRandIO coord
    p1 <- evalRandIO coord

    let l = line p0 p1

    s <- evalRandIO $ coords n

    let t = samples l s

    let xys = fromLists $ map (\(y, (Point x1 x2)) -> [fromIntegral y :: Double, 1.0, x1, x2]) t

    let xs = dropColumns 1 xys
    let ys = takeColumns 1 xys
      
    let w = (pinv $ (ctrans xs) <> xs) <> (ctrans xs) <> ys
      
    let y_train = xs <> w
        
    out_coords <- evalRandIO $ coords 1000
    
    let out_xs = fromLists $ map (\(Point x1 x2) -> [1.0, x1, x2]) out_coords

    let out_ys = out_xs <> w
        
    let actual_out = samples l out_xs
        
    return $ (w, ((sum $ map (\(yt, yi) -> (if (yt >= 0 && yi >= 0) || (yt < 0 && yi < 0) then 0 else 1)) $ zip (toList $ flatten ys) (toList $ flatten y_train)) / (fromIntegral n :: Double)))
  
main = do
    args <- getArgs
    let count = read $ args!!0 :: Int
    let n = read $ args!!1 :: Int

    e <- experiment n
    print e