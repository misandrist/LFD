import Control.Monad.Random
import Control.Monad
import Data.Packed.Matrix
import Numeric.LinearAlgebra.Algorithms
import Numeric.Container
import System.Environment
import Algebra.Linreg

data Point = Point Double Double
             deriving Show
             
coord :: (RandomGen g) => Rand g Point
coord = do
    x <- getRandom
    y <- getRandom

    return $ Point x y

line :: Point -> Point -> (Double -> Double)
line (Point x1 y1) (Point x2 y2) = (\x -> ((y2 - y1) / (x2 - x1)) * (x - x1) + y1)

lineRel :: (Double -> Double) -> Point -> Double
lineRel l (Point x y) = signum $ y - (l x)

coords :: (RandomGen g) => Int -> Rand g [Point]
coords n = sequence (replicate n coord)

samples :: (Double -> Double) -> [Point] -> [Sample]
samples l ps = map (\(Point x1 x2) ->
                        (Sample (lineRel l (Point x1 x2) :: Double) x1 x2)) ps

experiment :: Int -> IO Double
experiment n = do
  p0 <- evalRandIO coord
  p1 <- evalRandIO coord

  let l = line p0 p1

  s <- evalRandIO $ coords n

  let svec = sampleVec $ samples l s
  let xs = xVec svec
  let ys = yVec svec
      
  let w = wVec xs ys
    
  let y_train = xs <> w
    
  return $ (sum $
            map (\(yt, yi) -> (if (yt >= 0 && yi >= 0) || (yt < 0 && yi < 0) then 0 else 1)) $
            zip (toList $ flatten ys) (toList $ flatten y_train))
    / (fromIntegral n :: Double)
  
main = do
    args <- getArgs
    let count = read $ args!!0 :: Int
    let n = read $ args!!1 :: Int
    
    exp <- sequence (replicate count $ experiment n)

    print $ (sum exp) / (fromIntegral count :: Double)
    
