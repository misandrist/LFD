import Numeric.AD
import Options.Applicative
import Text.Printf
import Debug.SimpleReflect

data Params = Params {
    n :: Int,
    r :: Double,
    u0 :: Double,
    v0 :: Double}

params :: Parser Params
params = Params
         <$> option (long "n"
                     <> metavar "N"
                     <> help "The number of iterations")
         <*> option (long "r"
                     <> metavar "η"
                     <> help "The learning rate to apply to each gradient")
         <*> option (long "u0"
                     <> metavar "u0"
                     <> help "Initial value of u0.")
         <*> option (long "v0"
                     <> metavar "v0"
                     <> help "Initial value of v0.")

errfunc [u ,v] = ((u * exp v) - (2 * v * (exp (-u)))) ** 2

regress r [ux, vx] = map (\(x, dx) -> x - (r * dx)) $ zip [ux, vx] $ grad errfunc [ux, vx]

run :: Params -> IO ()
run (Params n r u0 v0) = do
  printf "N: %d η: %5.3f (u, v): (%5.3f, %5.3f)\n" n r u0 v0

  let i = drop (n - 2) $ take n $ iterate (\x -> regress r x) [u0, v0]

  let d = foldl (\[x1, y1] [x2, y2] -> [x2 - x1, y2 - y1]) [0, 0] i

  let f = drop 1 i

  print f
  print d

main = do
  execParser opts >>= run
      where opts = info (helper <*> params)
                   (fullDesc <>
                    progDesc "Perform gradient descent in the uv space." <>
                    header "Homework 5 problem 4")
